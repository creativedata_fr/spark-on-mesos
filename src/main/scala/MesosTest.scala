import org.apache.spark.{SparkContext, SparkConf}

/**
 * Created by aurelien on 15/07/15.
 */

object MesosTest {

  def main(args: Array[String]) {

    val sc = new SparkContext(new SparkConf().setAppName("MesosTest"))

    val file = sc.textFile("hdfs://176.31.116.73:8020/user/hdfs/us-constitution.txt")

    val counts = file.flatMap(line => line.toLowerCase().replace(".", " ").replace(",", " ").split(" ")).map(word => (word, 1L)).reduceByKey(_ + _)

    val sorted_counts = counts.collect().sortBy(wc => -wc._2)

    sorted_counts.take(10).foreach(println)

  }
}
