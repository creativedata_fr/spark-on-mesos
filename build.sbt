import sbt.Keys._

name := "spark-on-mesos"

version := "1.0"

scalaVersion := "2.11.7"

libraryDependencies += "org.apache.spark" %% "spark-core" % "1.4.0" % "provided"
